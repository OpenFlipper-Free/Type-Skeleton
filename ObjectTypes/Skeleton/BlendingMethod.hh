
#pragma once

namespace Blending {

  /**
   * @brief Possible deformation methods
   *
   * LBS = Linear Blend Skinning
   * DBS = Dual Quaternion Blend Skinning
   *
   */
  enum Method
  {
    M_LBS = 0,
    M_DBS = 1
  };

};

